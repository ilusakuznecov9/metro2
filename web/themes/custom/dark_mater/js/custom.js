(function ($) {
  $(document).ready(function() {

    if ($(window).width() > 992) {
      $('body').addClass('main-content-open');
    }

    $('#main-content-toggle').click(function(e) {
      $('body').toggleClass('main-content-open');
      e.stopPropagation();
      e.preventDefault();
    });

    $('#main-content-toggle-fab').click(function(e) {
      $('body').toggleClass('main-content-open');
      e.stopPropagation();
      e.preventDefault();
    });

    $('#share-toggle-fab').click(function(e) {
      e.stopPropagation();
      e.preventDefault();
    });

    $('.table-list tbody tr').click(function() {
      window.location = $(this).children('.views-field-title').children('a').attr('href');
    });

    $('.card-result').click(function() {
      window.location = $(this).children('.views-field-title').children('a').attr('href');
    });

    /*
    $('.root-msk a[href^="/statistics/"]').each(function() {
        this.href = this.href.replace('/statistics/', '/msk/statistics/');
    });

    $('.root-spb a[href^="/statistics/"]').each(function() {
        this.href = this.href.replace('/statistics/', '/spb/statistics/');
    });
    */

  }); 
})(jQuery);

// Theme fuctions with supporting ajax loading
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.darkMatterFunctions = {
    attach: function(context, settings) {

      $(context).find('.messages-close').click(function() {
        $(this).parent().slideUp('fast');
      });

      setTimeout(function() {
        $(context).find('.messages').slideUp('fast');
      }, 5000);

    }
  };
}) (jQuery, Drupal);
